# Дипломная работа к профессии Python-разработчик «API Сервис заказа товаров для розничных сетей».


## Задача
Необходимо разработать backend-часть (Django) сервиса заказа товаров для розничных сетей.
Базовая часть:

    Разработка сервиса под готовую спецификацию (API);
    Возможность добавления настраиваемых полей (характеристик) товаров;
    Импорт товаров;
    Отправка накладной на email администратора (для исполнения заказа);
    Отправка заказа на email клиента (подтверждение приема заказа).

Критерии достижения:

    Полностью работающие API Endpoint.
    Корректно отрабатывает следующий сценарий:
        пользователь может авторизоваться;
        есть возможность отправки данных для регистрации и получения email с подтверждением регистрации;
        пользователь может добавлять в корзину товары от разных магазинов;
        пользователь может подтверждать заказ с вводом адреса доставки;
        пользователь получает email с подтверждением после ввода адреса доставки;
        пользователь может переходить на страницу "Заказы" и открывать созданный заказ.



## Реализация базовой части
### Запуск проекта:

##### 1.    Добавление конфигурации СУБД, SMTP-сервера:
В папке <code>internet_shop</code>  в файле <code>settings.py</code>
указать стандартные параметры django для подключения к СУБД

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql',
            'NAME': 'final_diplom',
            'USER': 'login',
            'PASSWORD': 'password',
            'HOST': '127.0.0.1',
            'PORT': '5432',
        }
    }




В папке <code>utils</code> в файле <code>send_mail.py</code> нужно прописать параметры для подключения к SMTP-серверу.


    MAIL_SMTP_SERVER=servername
    MAIL_SMTP_LOGIN=login
    MAIL_SMTP_PASSWORD=password


Загрузить миграции:

    python manage.py migrate

Создать суперпользователя:
    
    python manage.py createsuperuser
    
    login: admin@admin.com
    password: admin

Запустить проект djago:

    python manage.py runserver

##### API Endpoints:

###### Авторизация пользователя:

 <code>/auth/login/</code>   <code>post</code>

    Body.json:

    {
        "email": "admin@admin.com",
        "password": "admin"
    }

![title](resourse/01_login.png)


###### Выход из авторизации:

<code>/auth/logout/</code>   <code>post</code>

    Header:
    Authorization: Token XXXXXXXXXXXXXXXXXXXXXXXXXXXXX

![title](resourse/02_logout.png)

###### Регистрация пользователя:

<code>/auth/registration/</code>     <code>post</code>
    
    Body.json:

    {
        "email": "testuser20021@address.com",
        "password1": "testuser20021@address.com",
        "password2": "testuser20021@address.com"
    }

![title](resourse/03_registration.png)

Поступает письмо на email зарегистрированного пользотателя

![title](resourse/04_registration.png)

###### Импорт YAML файла с продуктами:

<code>/import_price/</code>     <code>post</code>
    
    Header:
    Authorization: Token XXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    
    Body.form
    files: file.yaml

![title](resourse/05_import_price.png)


###### Продукты:

##### 1. Список

<code>/api/v1/product/</code>     <code>get</code>


![title](resourse/06_product_list.png)


##### 2. Детализация

<code>/api/v1/product/product_number</code>     <code>get</code>


![title](resourse/07_product_detail.png)



##### 3. Создание

<code>/api/v1/product/</code>     <code>post</code>

    Header:
    Authorization: Token XXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    
    Body.json:

    {
        "name": "Смартфон Apple iPhone XS Max 512GB (розовый)",
        "category": 224,
        "features": [
            {
                "feature": "Диагональ (дюйм)",
                "value": "6.5"
            },
            {
                "feature": "Разрешение (пикс)",
                "value": "2688x1242"
            },
            {
                "feature": "Встроенная память (Гб)",
                "value": "512"
            },
            {
                "feature": "Цвет",
                "value": "Розовый"
            }
        ],
        "assortment": [
            {
                "quantity": 11,
                "available": true,
                "description": 123,
                "price": 10000
            }
        ]
    }

При добавлении создается единица ассортимента + продукт, если такого не было ранее,
то есть продукт и магазин к который его продает его. опционально можно указать <code>features</code>.

##### 4. Изменение

<code>/api/v1/product/product_number</code>     <code>patch</code>

    Header:
    Authorization: Token XXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    
    Body.json:

    {
        "name": "Смартфон Apple iPhone XS Max 512GB (розовый)",
        "features": [
            {
                "id": 1,
                "feature": "Диагональ (дюйм)",
                "value": "6.6"
            },
            {
                "id": 2,
                "feature": "Разрешение (пикс)",
                "value": "2688x1242"
            },
            {
                "id": 3,
                "feature": "Встроенная память (Гб)",
                "value": "512"
            },
            {
                "id": 4,
                "feature": "Цвет",
                "value": "Розовый"
            }
        ],
        "category": 224,
        "assortment": [
            {
                "quantity": 11,
                "available": true,
                "price": "10000.00",
                "description": "1233"
            }
        ]
    }
    
##### 4. Удаление

<code>/api/v1/product/product_number</code>     <code>delete</code>

    Header:
    Authorization: Token XXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    
    
###### Характеристики:

##### 1. Список

<code>/api/v1/feature</code>     <code>get</code>

    Header:
    Authorization: Token XXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    
##### 1. Детализация

<code>/api/v1/feature/feature_number</code>     <code>get</code>

    Header:
    Authorization: Token XXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    
##### 3. Добавление

<code>/api/v1/feature/</code>     <code>post</code>

    Header:
    Authorization: Token XXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    
    Body.json
    
    {
        "name": "Втроенная память(Гб)"
    }
    
##### 4. Редактирование

<code>/api/v1/feature/feature_number/</code>     <code>patch</code>

    Header:
    Authorization: Token XXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    
    Body.json
    
    {
        "name": "Втроенная флэш память(Гб)"
    }
    
##### 5. Удаление

<code>/api/v1/feature/feature_number/</code>     <code>delete</code>

    Header:
    Authorization: Token XXXXXXXXXXXXXXXXXXXXXXXXXXXXX


###### Заказ:

Заказ в статусе <code>NEW</code> считается корзиной.

Как только переходит в <code>IN_PROGRESS</code> происходит отправка письма о заказе.

##### 1. Список наименований в новом заказе

<code>/api/v1/order-item</code>     <code>get</code>

    Header:
    Authorization: Token XXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    
Отдает список того, что лежит в заказе со статусом <code>NEW</code> текущего пользователя.
    
##### 2. Добавление пункта в заказ
Добавление происходит с корзину, т.е. в заказ в статусе <code>NEW</code>

<code>/api/v1/order-item/</code>     <code>post</code>

    Header:
    Authorization: Token XXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    
    Body.json
    
    {
        "assortment": {
            "id": 28
    },
        "quantity": 1
    }
    

##### 3. Изменение пункта заказа
Изменять можно количество 

<code>/api/v1/order-item/order-item_number/</code>     <code>patch</code>

    Header:
    Authorization: Token XXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    
    Body.json
    
    {
        "quantity": 4
    }
    
##### 4. Удаление

<code>/api/v1/order-item/order-item_number/</code>     <code>delete</code>

    Header:
    Authorization: Token XXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    
    
##### 5. Список заказов

<code>/api/v1/order/</code>     <code>get</code>

    Header:
    Authorization: Token XXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    
##### 6. детализация заказа

<code>/api/v1/order/order_number</code>     <code>get</code>

    Header:
    Authorization: Token XXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    
##### 7. Изменение заказа

<code>/api/v1/order/order_number</code>     <code>patch</code>

    Header:
    Authorization: Token XXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    
    Body.json
    
        {
        "status": "IN_PROGRESS",
        "recipient_email": "testuser2@mail.ru",
        "recipient_first_name": "test",
        "recipient_last_name": "test",
        "recipient_patronymic": "test",
        "recipient_phone": "9123455678",
        "profile": 4,
        "city": "Moscow",
        "street": "Paper Street",
        "house_number": 21,
        "housing": null,
        "structure": null,
        "apartment": "13",
        "additional_info": null,
        "order_items": [
            {
                "id": 5,
                "assortment": "Смартфон Apple iPhone XS Max 512GB (золотистый)",
                "quantity": 3,
                "price": 110000.0
            },
            {
                "id": 6,
                "assortment": "Смартфон Apple iPhone XR 256GB (красный)",
                "quantity": 10,
                "price": 65000.0
            }
        ]
    }
